/**
 * Created by brian on 7/28/16.
 */

$(document).ready(function() {

});

// Back to top scroller
$(document).on( 'click', 'a.back-to-top', function() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
    return false;
});

// Main Dropdowns
$(document).ready(function() {
    $(".header-menu").dropdowns();
});
