(function( $ ) {

    $.fn.dropdowns = function( options ) {

        $(this).addClass('dd-list');
        $(this).find('li:has(ul)').addClass('dd-trigger');
        $('.dd-trigger').hover(function() {
            $(this).find('ul').stop().slideToggle();
        });

    }

})(jQuery);