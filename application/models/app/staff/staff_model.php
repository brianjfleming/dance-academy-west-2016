<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_model extends CI_Model {
    
    public $primary_table;
    public $path;
	
    
	// ----------------------------------- //
	// Initialize Our Primary Table
	// ----------------------------------- //
	public function initialize($table,$path) {
		$this->primary_table = $table;	
        $this->path = $path;
	}
    
    public function renderTable() {
        $rstr = '';
        
        $rstr .= '<table class="fdcms-table sortable" width="100%" cellpadding="0" cellspacing="0">';
        $rstr .= $this->renderTableRows();
        
        $rstr .= '<tr>';
        $rstr .= '<td colspan="2">';
        $rstr .= '<img src="/images/app/core/arrow_ltr.png"> <a href="javascript: void(0);" class="check-all edit-link">Check</a> / <a href="javascript: void(0);" class="uncheck-all delete-link">Uncheck</a> All';
        $rstr .= '<a href="javascript:void(0);" class="delete delete-selected" data-post="'.$this->path.'/delete">Delete Selected</a>';
        $rstr .= '</td>';
        $rstr .= '</tr>';
        
        $rstr .= '</table>';
        
        $rstr .= '
            <script>
            var fixHelper = function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            };                
            $(".sortable tbody").sortable({
                helper: fixHelper,
                update: function(event, ui) {
                    var newOrder = $(this).sortable(\'toArray\').toString();
                    $.post(\'/admin/staff/all/order\', {order:newOrder});
                }
            }).disableSelection();
            </script>
        ';
        
        return $rstr;
    }

	// ----------------------------------- //
	// Row output for pages table
	// ----------------------------------- //
    public function renderTableRows() {
        $rstr = '';
        
        $this->db->from($this->primary_table);
        $this->db->order_by("staff_order", "asc");
        $query = $this->db->get();
        $count = $query->num_rows();
        $result = $query->result();
        
        if($count > 0) {
        
        foreach($result as $rs) {
            
            $class = ' class="native"';  
            
            $rstr .= '<tr id="'.$rs->staff_id.'">';
            
            // delete checkbox
            $rstr .= '<td width="20"'.$class.' valign="top">';
            $rstr .= '<input type="checkbox" name="'.$rs->staff_id.'" value="'.$rs->staff_id.'" id="box_'.$rs->staff_id.'" class="del-box" data-id="'.$rs->staff_id.'" data-post="'.$this->path.'/delete/'.$rs->staff_id.'">';
            $rstr .= '</td>';
            
            $rstr .= '<td'.$class.'>';
            $rstr .= '<div class="options-hover">';
            $rstr .= '<img src="'.$rs->staff_image.'" width="100" style="float: right; margin: 8px;">';
            $rstr .= $rs->staff_name.' - <i>'.$rs->staff_title.'</i>';
            $rstr .= '<span class="row-options">';
            $rstr .= '<a href="'.$this->path.'/edit/'.$rs->staff_id.'" class="edit-link">Edit</a>';
            $rstr .= ' | <a href="javascript:void(0)" class="delete-link delete-item" data-id="'.$rs->staff_id.'" data-post="'.$this->path.'/delete/'.$rs->staff_id.'">Delete</a>';
            $rstr .= '</span>';
            $rstr .= '</div>';
            $rstr .= '</td>';
            
            $rstr .= '</tr>';  
        }
        
        } else {
            $rstr .= '<tr><td>';
            $rstr .= '<div class="form-row"><div class="input-wrapper warning-text"><img src="/images/app/icons/warning.png" class="warning-icon"> <i>You haven\'t created any Staff Members to your website yet. Start by clicking "Add New Staff Member" on the right.</b></i><img src="/images/app/icons/help.png" class="help-icon help" data-subject="add-menus"></div></div>';   
            $rstr .= '</td></tr>';
        }
        
        return $rstr;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function create() {
        
        $data = array(
            'staff_id' => '0',
            'staff_name' => '',
            'staff_desc' => '',
            'staff_title' => '',
            'staff_image' => ''
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/staffEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function edit() {
        $id = $this->uri->segment(5);
        
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE staff_id = '".$id."' LIMIT 1";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        $data = array(
            'staff_id' => $row->staff_id,
            'staff_name' => $row->staff_name,
            'staff_desc' => $row->staff_desc,
            'staff_title' => $row->staff_title,
            'staff_image' => $row->staff_image
        );
        
        $rstr = '';
        $rstr .= '<div class="content-container">';
        $rstr .= $this->load->view('appForms/staffEditForm', $data, true);
        $rstr .= '</div>';
        
        return $rstr;   
    }

	// ----------------------------------- //
	// Saves and Creates pages
	// ----------------------------------- //
    public function save($image) {
        $id = $this->uri->segment(5);
        
        if($image == '') {
			$image = $this->input->post('staff_image_current');	
		} else {
            $this->core_model->deleteFile($this->input->post('staff_image_current'));   
        }
        
        $data = array(
            'staff_id' => $this->input->post('staff_id'),
            'staff_name' => $this->input->post('staff_name'),
            'staff_desc' => $this->input->post('staff_desc'),
            'staff_title' => $this->input->post('staff_title'),
            'staff_image' => $image
        );
        
        
        if($id == 0) {          
            $this->db->insert($this->primary_table,$data);
			$return["id"] = $this->db->insert_id();
            $return["message"] = "The New Staff Member [ ".$data["staff_name"]." ] was created successfully.";
        } else {
            // Saving an existing Page    
            $this->db->where('staff_id',$id);
			$this->db->update($this->primary_table,$data); 
            $return["id"] = $id;
            $return["message"] = "Your changes have been saved.";
        }
        
        return $return;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function getName($id) {
        $SQL = "SELECT * FROM ".$this->primary_table." WHERE staff_id = '".$id."'";
        $query = $this->db->query($SQL);
        $row = $query->row();
        
        return $row->staff_name;
    }

	// ----------------------------------- //
	// Create New Page Function
	// ----------------------------------- //
    public function editToolbox($id) {
        
        $toolboxArray = array();
		
        return $toolboxArray;
    }
    
    
    
    
}

?>