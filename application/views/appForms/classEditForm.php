<form id="pageForm" name="pageForm" method="post" action="/admin/classes/all/save/<? echo $class_id; ?>" enctype="multipart/form-data">
    <input type="hidden" name="class_id" id="class_id" value="<? echo $class_id; ?>">
	<div class="form-row">
    <div class="input-wrapper">
    <label for="class_name">Class Name</label>
    <input type="text" name="class_name" id="class_name" value="<? echo $class_name; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="class_desc">Class Summary</label>
    <textarea name="class_desc" id="class_desc"><? echo $class_desc; ?></textarea>
    </div>
    </div>
    
    <!-- --------------------- -->
    
	<div class="form-row">
    <div class="input-wrapper">
    <input type="hidden" name="class_image_current" id="class_image_current" value="<? echo $class_image; ?>">
    <label for="class_image">Image <span class="small">(480 x 490 Pixels)</span></label>
    <? if($class_image != '') { echo '<img src="'.$class_image.'" style="float: left; margin-right: 18px;" width="150">'; } else { } ?>
    <input type="file" name="file" id="file" class="input-full" />
    <div class="clear"></div>
    </div>
    </div>
    
    <!-- --------------------- -->
</form>