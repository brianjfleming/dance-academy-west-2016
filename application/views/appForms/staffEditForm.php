<form id="pageForm" name="pageForm" method="post" action="/admin/staff/all/save/<? echo $staff_id; ?>" enctype="multipart/form-data">
    <input type="hidden" name="staff_id" id="staff_id" value="<? echo $staff_id; ?>">
	<div class="form-row">
    <div class="input-wrapper">
    <label for="staff_name">Name</label>
    <input type="text" name="staff_name" id="staff_name" value="<? echo $staff_name; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->
    
	<div class="form-row">
    <div class="input-wrapper">
    <label for="staff_title">Title</label>
    <input type="text" name="staff_title" id="staff_title" value="<? echo $staff_title; ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="staff_desc">Bio</label>
    <textarea name="staff_desc" id="staff_desc"><? echo $staff_desc; ?></textarea>
    </div>
    </div>
    
    <!-- --------------------- -->
    
	<div class="form-row">
    <div class="input-wrapper">
    <input type="hidden" name="staff_image_current" id="staff_image_current" value="<? echo $staff_image; ?>">
    <label for="staff_image">Image <span class="small">(313 x 313 Pixels)</span></label>
    <? if($staff_image != '') { echo '<img src="'.$staff_image.'" style="float: left; margin-right: 18px;" width="150">'; } else { } ?>
    <input type="file" name="file" id="file" class="input-full" />
    <div class="clear"></div>
    </div>
    </div>
    
    <!-- --------------------- -->
</form>