<?
if(isset($cta_image)) {
echo '<div class="current-image">';
echo '<img src="'.$cta_image.'">';
echo '</div>';    
}
?>


<form id="pageForm" name="pageForm" method="post" action="/admin/pages/all/ctasave/<? echo $page_id; ?>" enctype="multipart/form-data">

	<div class="form-row">
    <div class="input-wrapper">
    <label for="file">
    Upload New Image: <span class="small">( Optional )</span></label>
    <input type="file" name="file" id="file" value="" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="cta_title">Title</label>
    <input type="text" name="cta_title" id="cta_title" value="<? if(isset($cta_title)) { echo $cta_title; } ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="cta_text">Text</label>
    <input type="text" name="cta_text" id="cta_text" value="<? if(isset($cta_text)) { echo $cta_text; } ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->

	<div class="form-row">
    <div class="input-wrapper">
    <label for="cta_link">Link</label>
    <input type="text" name="cta_link" id="cta_link" value="<? if(isset($cta_link)) { echo $cta_link; } ?>" class="input-full" />
    </div>
    </div>
    
    <!-- --------------------- -->
</form>

<script type="text/javascript">
    $(document).ready(function() {
       
    });
    
    function convertData() {     
        // Make sure out links are local
        var baseURL = '<?php echo BASE_URL(); ?>'; 
        var str = $('#cta_link').val();
        str = str.replace(''+baseURL+'','/');
        $('#cta_link').val(str);   
    }
</script>