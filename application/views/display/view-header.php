<div class="header row">
    <div class="wrapper">
        <div class="logo">
            <a href="/"><img src="/images/display/header/daw2016_logo.gif" /></a>
        </div>
        <div class="social">
            <ul>
                <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="navigation">
            <? $fdcms->nav_menu('primary',true,'header-menu'); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>