<?
// Template Name: Homepage
?>

<!doctype html>
<html>
<head>
    <? $this->load->view('display/view-head'); ?>
    <link rel="stylesheet" href="/css/display/homepage.css" type="text/css">
</head>

<body>
<div class="page">

	<? $this->load->view('display/view-header'); ?>

    <div class="slideshow row">
        <? $fdcms->render_slideshow(); ?>
    </div>

    <div class="cta row">
        <div class="wrapper">
        
        <? $fdcms->renderCTAs(); ?>
        
        </div>
    </div>

    <div class="video row">
        VIDEO
    </div>

    <div class="content row">
        <div class="wrapper">
            <h2><? $fdcms->the_subtitle(); ?></h2>

            <div class="daw-divider"></div>

            <? $fdcms->html_block("Main Content"); ?>
        </div>
    </div>

    <div class="testimonials row">
        <div class="wrapper">

            <div class="testimonial-title"><span>TESTIMONIALS</span></div>
            <? $fdcms->testimonials_slider(); ?>

        </div>
    </div>

    <div class="marketing row">
        <div class="wrapper">

            <div class="left thirty contact-info">
                <h3>Contact Information</h3>
                <? $fdcms->html_block("Contact Information"); ?>
            </div>
            <div class="right seventy marketing-text">
                <h3>Marketing Heading</h3>
                <? $fdcms->html_block("Marketing Content"); ?>
            </div>
            <div class="clear"></div>

        </div>
    </div>
    
	<? $this->load->view('display/view-footer'); ?>

</div>
</body>
</html>
