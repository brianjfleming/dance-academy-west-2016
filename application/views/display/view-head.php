<meta charset="utf-8">
<title><? $fdcms->meta_title(); ?></title>
<meta name="description" content="<? $fdcms->meta_desc(); ?>">
<meta name="viewport" content="initial-scale=1">

<!-- REQUIRED by SYSTEM -->
<!-- ------------------ -->
<script type="text/javascript" src="/js/system/jquery.2.1.1.min.js"></script>
<script type="text/javascript" src="/js/system/jquery.modernizr.min.js"></script>
<script type="text/javascript" src="/js/display/master.js"></script>
<script type="text/javascript" src="/js/display/jquery.dropdowns.js"></script>
<link rel="stylesheet" type="text/css" href="/css/system/system.css">
<link rel="stylesheet" type="text/css" href="/css/system/boilerplate.css">
<link rel="stylesheet" type="text/css" href="/css/system/font-awesome.min.css">
<!-- ------------------ -->
<!--  END REQUIREMENTS  -->

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

<!-- Master CSS File -->
<link rel="stylesheet" type="text/css" href="/css/display/master.css">