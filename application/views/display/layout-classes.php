<?
// Template Name: Class Descriptions Page
?>

<!doctype html>
<html>
<head>
    <? $this->load->view('display/view-head'); ?>
    <link rel="stylesheet" href="/css/display/classes.css" type="text/css">
</head>

<body>
<div class="page">

	<? $this->load->view('display/view-header'); ?>

    <div class="slideshow row">
        <? $fdcms->render_slideshow(); ?>
    </div>

    <div class="content row">
        <div class="wrapper">

            <div id="sidebar" class="left thirty">
                <?
                $url = $this->uri->segment(1);
                $fdcms->nav_menu($url,true,'sidebar-nav');
                ?>
            </div>
            <div id="content-sidebar" class="right seventy">
                <h1><? $fdcms->the_subtitle(); ?></h1>
                <? $fdcms->html_block("Main Content"); ?>
                <? $fdcms->renderClasses(); ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    
	<? $this->load->view('display/view-footer'); ?>

</div>
</body>
</html>
